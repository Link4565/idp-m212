#include <Arduino_LSM6DS3.h>
double acc_list[7] = {};
double av_acc_list[5] = {};
double diff_av_acc_list[4] = {};
double inte_diff_av_acc_list[3] = {};
double vel_list[2] = {};
double distance = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial);

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");

    while (1);
  }

  

}

void loop() {
  // put your main code here, to run repeatedly:
  float x, y, z;

  IMU.readAcceleration(x,y,z);
  acc_list[0] = acc_list[1];
  acc_list[1] = acc_list[2];
  acc_list[2] = acc_list[3];
  acc_list[3] = acc_list[4];
  acc_list[4] = acc_list[5];
  acc_list[5] = acc_list[6];
  acc_list[6] = x;
  av_acc_list[0] = (acc_list[0] + acc_list[1]+ acc_list[2])/3;
  av_acc_list[1] = (acc_list[1] + acc_list[2]+ acc_list[3])/3;
  av_acc_list[2] = (acc_list[2] + acc_list[3]+ acc_list[4])/3;
  av_acc_list[3] = (acc_list[3] + acc_list[4]+ acc_list[5])/3;
  av_acc_list[4] = (acc_list[4] + acc_list[5]+ acc_list[6])/3;
  diff_av_acc_list[0] = (av_acc_list[1] - av_acc_list[0])/1;
  diff_av_acc_list[1] = (av_acc_list[2] - av_acc_list[1])/1;
  diff_av_acc_list[2] = (av_acc_list[3] - av_acc_list[2])/1;
  diff_av_acc_list[3] = (av_acc_list[4] - av_acc_list[3])/1;
  inte_diff_av_acc_list[0] = inte_diff_av_acc_list[0] + 0.5*(diff_av_acc_list[0] + diff_av_acc_list[1])*1;
  inte_diff_av_acc_list[1] = inte_diff_av_acc_list[1] + 0.5*(diff_av_acc_list[1] + diff_av_acc_list[2])*1;
  inte_diff_av_acc_list[2] = inte_diff_av_acc_list[2]+ 0.5*(diff_av_acc_list[2] + diff_av_acc_list[3])*1;
  Serial.print(inte_diff_av_acc_list[0]);
  Serial.print(inte_diff_av_acc_list[1]);
  Serial.println(inte_diff_av_acc_list[2]);
  vel_list[0] = vel_list[0] + 0.5*(inte_diff_av_acc_list[0] + inte_diff_av_acc_list[1] ) *1;
  vel_list[1] = vel_list[1] + 0.5*(inte_diff_av_acc_list[1] + inte_diff_av_acc_list[2] ) *1;
  Serial.print(vel_list[0]);
  Serial.println(vel_list[1]);
  distance = distance + 0.5*(vel_list[0] + vel_list[1])*1;
  Serial.println(distance);
  
  delay(1000);
  

}
