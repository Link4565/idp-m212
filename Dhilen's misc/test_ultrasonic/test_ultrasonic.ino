#include <HCSR04_with_timeout.h>

// Initialize sensor that uses digital pins 13 and 12.
int triggerPin = 7;
int echoPin = 8;
UltraSonicDistanceSensor ultrasonic(triggerPin , echoPin);

double distance; 

void setup () {
    Serial.begin(115200);  // We initialize serial connection so that we could print values from sensor.
}

void loop () {
  distance = ultrasonic.measureDistanceCm();
  Serial.println(distance);
  
  
  
}
