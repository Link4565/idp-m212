/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *motorL = AFMS.getMotor(1);
Adafruit_DCMotor *motorR = AFMS.getMotor(2);

Servo hand;
Servo wrist;
// create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position
int x0;
int x1;
int x2;
int x3;

void setup() {
  hand.attach(9);
  wrist.attach(10);
  hand.write(50);
  wrist.write(120);
  // attaches the servo on pin 9 to the servo object
  Serial.begin(9600);

  motorL->setSpeed(200);
  motorR->setSpeed(200);
}

void loop() {
  
  for (pos = 120; pos >= 70; pos -= 1) { // goes from 180 degrees to 0 degrees
    hand.write(pos);              // tell servo to go to position in variable 'pos'
    x0 = hand.read();
    Serial.println(x0);
    delay(15);                       // waits 15ms for the servo to reach the position
  }

  for (pos = 35; pos <= 100; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    wrist.write(pos); 
    x1 = wrist.read();
    Serial.println(wrist.read());// tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }


  /*motorL->run(FORWARD);
  motorR->run(FORWARD);*/
  

   for (pos = 100; pos >= 35; pos -= 1) { // goes from 180 degrees to 0 degrees
    wrist.write(pos); 
    x2 = wrist.read();
    Serial.println(x2);// tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }  
  
  for (pos = 70; pos <= 120; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    hand.write(pos);   
    x3 = hand.read();
    Serial.println(x3);// tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  
}
