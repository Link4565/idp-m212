#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *motorL = AFMS.getMotor(1);
Adafruit_DCMotor *motorR = AFMS.getMotor(2);

const int left_sensor = A0;
const int right_sensor = A1;


void setup() {
  // put your setup code here, to run once:
  pinMode (left_sensor, INPUT);
  pinMode(right_sensor, INPUT);
  motorL->setSpeed(100);
  motorR->setSpeed(100);

  AFMS.begin();

}

void loop() {
  // put your main code here, to run repeatedly:
  if (left_sensor < 500 and right_sensor >500){
    motorL -> run(FORWARD);
    motorR-> run(RELEASE);
    }

  if (left_sensor>500 and right_sensor<500){
    motorL ->run(RELEASE);
    motorR ->run(FORWARD);
   }

  else{
    motorL ->run(FORWARD);
    motorR->run(FORWARD);
  }

}
