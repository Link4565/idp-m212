#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>


Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *motorL = AFMS.getMotor(1);
Adafruit_DCMotor *motorR = AFMS.getMotor(2);

Servo hand;
Servo wrist;

void setup() {
  // put your setup code here, to run once:

AFMS.begin();

hand.attach(9); 
wrist.attach(10);
hand.write(180);
wrist.write(35);




}

void loop() {
  // put your main code here, to run repeatedly:
  motorL->run(RELEASE);
  motorR->run(RELEASE);





}
