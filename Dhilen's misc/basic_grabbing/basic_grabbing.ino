#include <HCSR04.h>
#include <Servo.h>

Servo pinch;
Servo lift;

int triggerPin = 2;
int echoPin = 3;

UltraSonicDistanceSensor distanceSensor(triggerPin, echoPin);

void setup() {
  // put your setup code here, to run once:
  double distance = distanceSensor.measureDistanceCm();
  int distance_list[] = {0};
  pinch.attach(9);
  lift.attach(10);
  pinch.write(90);
  lift.write(90);


}

void loop() {
  // put your main code here, to run repeatedly:

  if (distance_list.size()<2 and distance >= 0){
    distance_list.push_back(distance);
  }
  
  else if (distance_list.size() == 2 and distance >= 0){
    
    distance_list.push_back(distance);
    Serial.println(distance_list);
    
    float sum = 0;
    for (int i = 0; i < 3; i++){
      sum += distance_list[i];
    }
    float average_dist = sum/3;

    
    distance_list.remove(distance_list[0]);

  }
  
  if (average_dist <7){
    motorLspeed = 0;
    motorRspeed = 0;

    delay(2000);
    for (pos = 0; pos <= 50; pos += 1) { 
    pinch.write(pos); 
    }
    delay(5000);
    for (pos = 0; pos <= 90; pos += 1) { 
    lift.write(pos);               
    }     
    delay(2000);         
  
    
  }
}
