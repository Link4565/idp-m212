
int lineSensorL = 2;
int lineSensorC = 3;
int lineSensorR = 4;

void setup() {
  // put your setup code here, to run once:
  pinMode(lineSensorL,INPUT);
  pinMode(lineSensorC,INPUT);
  pinMode(lineSensorR,INPUT);
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print('L');
  Serial.print(digitalRead(lineSensorL));
  //delay(500);
  Serial.print('C');
  Serial.print(digitalRead(lineSensorC));
  // delay(500);
  Serial.print('R');
  Serial.println(digitalRead(lineSensorR));
   //delay(500);
  

}
