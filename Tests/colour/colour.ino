
int redVoltage, blueVoltage;

float redness;//R, B, B2,


// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pins:
  redVoltage = analogRead(A1);
  blueVoltage = analogRead(A0);

  //R = intensity(redVoltage);
  //B = intensity(blueVoltage);


  //B2 = B * 5;

  //redness = (R-B2)/(R+B2);

  Serial.println(String(redVoltage) + "    " + String(blueVoltage));
  //Serial.println(String(R) + "    " + String(B));

  redness = getRedness();
  
  Serial.println(redness);
  
  if (redness >0.1) {Serial.println("red");}
  else if (redness < -0.0)  {Serial.println("blue");}
  else  {Serial.println("neutral");}

  
  delay(10);        // delay in between reads for stability
}

float getRedness(){

  float R = 0.2+intensity(analogRead(A1));
  float B = 0.2+5*intensity(analogRead(A0));

  return (R-B)/(R+B);
  
}

float intensity(int v){
  return (1023.0/float(v)-1)/0.1; //Convert voltage to light intensity
}
