#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *motorL = AFMS.getMotor(1);
Adafruit_DCMotor *motorR = AFMS.getMotor(2);


void setup() {
  // put your setup code here, to run once:

AFMS.begin();

motorL->setSpeed(100);
motorR->setSpeed(100);

}

void loop() {
  // put your main code here, to run repeatedly:

motorL->run(FORWARD);
motorR->run(FORWARD);


/*
motorL->run(RELEASE);
motorR->run(RELEASE);
*/

//delay(2000);
/*
motorL->run(BACKWARD);
motorR->run(BACKWARD);

delay(2000);

motorL->run(RELEASE);
motorR->run(RELEASE);

delay(2000);
*/

}
