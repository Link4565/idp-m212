import numpy as np
import cv2
from cv2 import aruco
import yaml
import imutils
import serial
import json
import time

#Load calibration data from file
with open('calibration_matrix.yaml') as f:
    my_dict = yaml.safe_load(f)

#Open connection to camera
cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")
#cap = cv2.VideoCapture(1)

#Load calibration data into variables
mtx = np.asarray(my_dict["camera_matrix"])
dst = np.asarray(my_dict["dist_coeff"])

i=1

#Initialise variables
step = 1
set_mode = [-1,-1]
motor_speeds = [0,0]

#Wait until Droid Boi is connected over bluetooth before continuing
input("Once connected to Droid Boi, Press Enter to start the serial connection:")

#Open serial conenction to Droid Boi
ser = serial.Serial("COM8", baudrate= 9600, timeout=2.5, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE)

#input("Press Enter to start the run:")

#ser.write("s".encode('ascii'))

#Wait for user to call robot back to start
input("Press Enter to call the robot back to the start:")


while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    #Remove the image distortion from the wide-angle lens
    frame = cv2.undistort(frame,mtx,dst,None,mtx)

    angle = -5
    frame = imutils.rotate_bound(frame, angle)

    #Convert the video to greyscale for processing
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #Find any ArUco markers in the frame (a marker is attached to the top of Droid Boi)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    #Draw boxes around the ArUco markers onto the video feed
    frame_markers = aruco.drawDetectedMarkers(frame.copy(), corners, ids)

    #Only try to update position readings if Droid Boi is found
    lost_tracking = True
    if (len(corners)>0):
        lost_tracking = False
        #print(corners[0])
        #print(corners[0][0][0])
        #print((corners[0][0][0][0]+corners[0][0][1][0]+corners[0][0][2][0]+corners[0][0][3][0])/4)
        #print((corners[0][0][0][1]+corners[0][0][1][1]+corners[0][0][2][1]+corners[0][0][3][1])/4)

        #Calculate the position and orientation of Droid Boi from the ArUco corner locations
        position = [(corners[0][0][0][0]+corners[0][0][1][0]+corners[0][0][2][0]+corners[0][0][3][0])/4,(corners[0][0][0][1]+corners[0][0][1][1]+corners[0][0][2][1]+corners[0][0][3][1])/4]
        print(position)
        direction = np.degrees(np.arctan2((-corners[0][0][0]+position)[1],(-corners[0][0][0]+position)[0]))-45
        #print(direction)
    
    #Comeplete actions in sequence using the step variable
    if step == 1:
        #Do not run if the robot has yet to be found
        if position is not None:
            #If on the side of the table with the loop
            if position[0]<400:
                #Calculate angle needed to turn by to point towards the tunnel entrance
                direction_to_travel = (360-np.degrees(np.arctan2((-235+position[1]),(207-position[0])))+90)%360
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                #Move on to step 2 if next to the tunnel entrance
                if abs(position[0]-207)<10 and abs(position[1]-235)<10:
                    step = 2
            else:
                #Calculate angle needed to turn by to point towards the start box
                direction_to_travel = (360-np.degrees(np.arctan2((-189+position[1]),(489-position[0])))+90)%360
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                #Move on to step 2 if by the start box
                if abs(position[0]-489)<10 and abs(position[1]-189)<10:
                    step = 2

        #Foll forwards in an arc to regain tracking if marker can't be seen in current posiiton and orientation
        if lost_tracking:
            motor_speeds = [150,170]
        else:
            #Rotate towards the given location if more than 10 degrees off axis
            if abs(angle_difference) > 10:
                if (angle_difference > 0):
                    motor_speeds = [150,-150]
                if (angle_difference < 0):
                    motor_speeds = [-150,150]
            #Drive towards the given location
            else:
                motor_speeds = [150,150]

    if step == 2:
        if position is not None:
            if position[0]<400:
                #Point directly into the tunnel
                direction_to_travel = 90
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                if abs(angle_difference)<10:
                    step = 3
            else:
                #Point at 90 degrees to the line underneath
                direction_to_travel = -90
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                if abs(angle_difference)<10:
                    step = 3

        if lost_tracking:
            motor_speeds = [150,-170]
        else:
            if abs(angle_difference) > 5:
                if (angle_difference > 0):
                    motor_speeds = [150,-150]
                if (angle_difference < 0):
                    motor_speeds = [-150,150]

    if step == 3:
        if position is not None:
            if position[0]<400:
                #Point into the tunnel
                direction_to_travel = 90
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                #When in the tunnel and on the line, switch to line following to the start box
                if position[0]>270 and angle_difference<5:
                    motor_speeds = [0,0]
                    set_mode = [1,5]
                    step = 4
            else:
                #Rotate clockwise until on the line, then line follow into the start box
                motor_speeds = [0,0]
                set_mode = [5,13]
                step = 4

        if lost_tracking:
            motor_speeds = [150,-170]
        else:
            if abs(angle_difference) > 5:
                if (angle_difference > 0):
                    motor_speeds = [150,-150]
                if (angle_difference < 0):
                    motor_speeds = [-150,150]
            else:
                motor_speeds = [150,150]

    #Create dictionary of motor speeds and/or modes to send to Droid Boi
    data = {}
    data["m"] = motor_speeds
    data["s"] = set_mode
    #Convert dict to serial data
    data = json.dumps(data)
    #Write data to serial
    if ser.isOpen():
        ser.write(data.encode('ascii'))
        ser.flush()

    #Close connection to serial and stop performing actions
    if step == 4:
        ser.close()
        step = 0

    #Delay to prevent sending commands to the Arduino to frequently, and save laptop battery life
    time.sleep(0.2)


    # Display the current video feed with markers overlayed
    cv2.imshow("frame - Press 'q' to close",frame_markers)
    #cv2.imshow('frame',gray)

    #Save the current image, or exit program
    key = cv2.waitKey(1) & 0xFF
    if key == ord('s'):
        cv2.imwrite('./Images/photo{}.jpg'.format(i),frame_markers)
        i+=1
    if key == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()