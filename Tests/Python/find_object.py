import numpy as np
import cv2

#cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")
#cap = cv2.VideoCapture("http://10.113.97.50:4747/video")
cap = cv2.VideoCapture(1)

MIN_MATCH_COUNT = 10

i=1
while(True):
    # Capture frame-by-frame
    ret, image = cap.read()
    #image = cv2.imread("qr-code.png")
    frame = cv2.imread("Selwyn_College_shield.svg.png")

    # Initiate SIFT detector
    sift = cv2.ORB_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(frame,None)
    kp2, des2 = sift.detectAndCompute(image,None)

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = []

    #matches = flann.knnMatch(des1,des2,k=2)
    if des1 is not None and des2 is not None:
        if len(des1)>=2 and len(des2)>=2:
            matches=flann.knnMatch(np.asarray(des1,np.float32),np.asarray(des2,np.float32), k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()

        h,w = frame.shape[0:2]
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        if M is not None:
            dst = cv2.perspectiveTransform(pts,M)
        else:
            dst = pts

        image = cv2.polylines(image,[np.int32(dst)],True,255,3, cv2.LINE_AA)

    else:
        print("Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT))
        matchesMask = None

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                    singlePointColor = None,
                    matchesMask = matchesMask, # draw only inliers
                    flags = 2)

    img3 = cv2.drawMatches(frame,kp1,image,kp2,good,None,**draw_params)

    # Display the resulting frame
    cv2.imshow("frame - Press 'q' to close",img3)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('s'):
        cv2.imwrite('./Images/photo{}.jpg'.format(i),img3)
        i+=1
    if key == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()