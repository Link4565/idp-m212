import numpy as np
import cv2

def cascade(frame):
    rectangles = cv2.CascadeClassifier("Cascade Training/cascade/cascade.xml").detectMultiScale(frame)
    return rectangles


#cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")
#cap = cv2.VideoCapture("http://10.113.97.50:4747/video")
cap = cv2.VideoCapture(1)

MIN_MATCH_COUNT = 10

tracker = cv2.TrackerCSRT_create()

initBB = None

j=0
i=1
while(True):
    j+=1
    # Capture frame-by-frame
    ret, frame = cap.read()

    (H, W) = frame.shape[:2]

    # check to see if we are currently tracking an object
    if initBB is not None and j<20:
        # grab the new bounding box coordinates of the object
        (success, box) = tracker.update(frame)

        # check to see if the tracking was a success
        if success:
            (x, y, w, h) = [int(v) for v in box]
            cv2.rectangle(frame, (x, y), (x + w, y + h),
                (0, 255, 0), 2)
        else:
            initBB = None
    else:
        print("test")
        rectangles = cascade(frame)

        if rectangles is not None:
            for i in rectangles:
                print(tuple(i))
                cv2.rectangle(frame, tuple(i), (255,0,0), thickness=1, lineType=8, shift=0)
                initBB = tuple(i)
            tracker = cv2.TrackerCSRT_create()
            tracker.init(frame, initBB)
            j=0

    # Display the resulting frame
    cv2.imshow("frame - Press 'q' to close",frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('s'):
        cv2.imwrite('./Images/photo{}.jpg'.format(i),frame)
        i+=1
    if key == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()