import numpy as np
import cv2
import yaml
import imutils
import time

with open('calibration_matrix.yaml') as f:
    my_dict = yaml.safe_load(f)

cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")

mtx = np.asarray(my_dict["camera_matrix"])
dst = np.asarray(my_dict["dist_coeff"])

i=38
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    frame = cv2.undistort(frame,mtx,dst,None,mtx)

    angle = -5
    frame = imutils.rotate_bound(frame, angle)

    # Display the resulting frame
    cv2.imshow("frame - Press 'q' to close",frame)
    #cv2.imshow('frame',gray)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('s'):
        cv2.imwrite('./Images/photo{}.jpg'.format(i),frame)
        i+=1
    if key == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()