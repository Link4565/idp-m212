import numpy as np
import cv2

#cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")
cap = cv2.VideoCapture(0)

i=1
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    image = cv2.imread("qr-code.png")

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    retval, points = cv2.QRCodeDetector().detect(frame)
    if points is not None:
        pts = len(points)
        print(pts)
        for i in range(pts):
            nextPointIndex = (i+1) % pts
            cv2.line(frame, tuple(points[i][0]), tuple(points[nextPointIndex][0]), (255,0,0), 5)
            print(points[i][0]) 

    # Display the resulting frame
    cv2.imshow("frame - Press 'q' to close",frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('s'):
        cv2.imwrite('./Images/photo{}.jpg'.format(i),frame)
        i+=1
    if key == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()