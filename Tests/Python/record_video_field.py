import numpy as np
import cv2
import time

cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")

second = 0
i=1
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow("frame - Press 'q' to close",frame)
    #cv2.imshow('frame',gray)

    if int(time.time()%30) != second:
        cv2.imwrite('./Images/photo{}.jpg'.format(i),frame)
        i+=1
        second = int(time.time()%30)

    key = cv2.waitKey(1) & 0xFF
    #if key == ord('s'):
    #    cv2.imwrite('./Images/photo{}.jpg'.format(i),frame)
    #    i+=1
    if key == ord('q'):
        break


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()