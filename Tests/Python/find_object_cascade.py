import numpy as np
import cv2

#cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")
#cap = cv2.VideoCapture("http://10.113.97.50:4747/video")
cap = cv2.VideoCapture(1)

MIN_MATCH_COUNT = 10

i=1
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    rectangles = cv2.CascadeClassifier("Cascade Training/cascade/cascade.xml").detectMultiScale(frame)

    if rectangles is not None:
        for i in rectangles:
            print(tuple(i))
            cv2.rectangle(frame, tuple(i), (255,0,0), thickness=1, lineType=8, shift=0)

    # Display the resulting frame
    cv2.imshow("frame - Press 'q' to close",frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('s'):
        cv2.imwrite('./Images/photo{}.jpg'.format(i),frame)
        i+=1
    if key == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()