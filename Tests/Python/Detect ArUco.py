import numpy as np
import cv2
from cv2 import aruco
import serial
import json
import time

#cap = cv2.VideoCapture("http://localhost:8081/stream/video.mjpeg")
cap = cv2.VideoCapture(1)

i=1

step = 1
set_mode = [-1,-1]
motor_speeds = [0,0]

input("Once connected to Droid Boi, Press Enter to start the serial connection:")

ser = serial.Serial("COM8", baudrate= 9600, timeout=2.5, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE)

#input("Press Enter to start the run:")

#ser.write("s".encode('ascii'))

input("Press Enter to call the robot back to the start:")

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    frame_markers = aruco.drawDetectedMarkers(frame.copy(), corners, ids)

    lost_tracking = True
    if (len(corners)>0):
        lost_tracking = False
        #print(corners[0])
        #print(corners[0][0][0])
        #print((corners[0][0][0][0]+corners[0][0][1][0]+corners[0][0][2][0]+corners[0][0][3][0])/4)
        #print((corners[0][0][0][1]+corners[0][0][1][1]+corners[0][0][2][1]+corners[0][0][3][1])/4)
        position = [(corners[0][0][0][0]+corners[0][0][1][0]+corners[0][0][2][0]+corners[0][0][3][0])/4,(corners[0][0][0][1]+corners[0][0][1][1]+corners[0][0][2][1]+corners[0][0][3][1])/4]
        print(position)
        direction = np.degrees(np.arctan2((-corners[0][0][0]+position)[1],(-corners[0][0][0]+position)[0]))-45
        #print(direction)
    
    if step == 1:
        if position is not None:
            if position[0]<400:
                direction_to_travel = (360-np.degrees(np.arctan2((-235+position[1]),(207-position[0])))+90)%360
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                if abs(position[0]-207)<10 and abs(position[1]-235)<10:
                    step = 2
            else:
                direction_to_travel = (360-np.degrees(np.arctan2((-189+position[1]),(489-position[0])))+90)%360
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                if abs(position[0]-489)<10 and abs(position[1]-189)<10:
                    step = 2

        if lost_tracking:
            motor_speeds = [150,170]
        else:
            if abs(angle_difference) > 10:
                if (angle_difference > 0):
                    motor_speeds = [150,-150]
                if (angle_difference < 0):
                    motor_speeds = [-150,150]
            else:
                motor_speeds = [150,150]

    if step == 2:
        if position is not None:
            if position[0]<400:
                direction_to_travel = 90
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                if abs(angle_difference)<10:
                    step = 3
            else:
                direction_to_travel = -90
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                if abs(angle_difference)<10:
                    step = 3

        if lost_tracking:
            motor_speeds = [150,-170]
        else:
            if abs(angle_difference) > 5:
                if (angle_difference > 0):
                    motor_speeds = [150,-150]
                if (angle_difference < 0):
                    motor_speeds = [-150,150]

    if step == 3:
        if position is not None:
            if position[0]<400:
                direction_to_travel = 90
                angle_difference = ((direction_to_travel - direction) +180) % 360 -180
                #print(direction_to_travel)
                print(angle_difference)

                if position[0]>270 and angle_difference<5:
                    motor_speeds = [0,0]
                    set_mode = [1,5]
                    step = 4
            else:
                motor_speeds = [0,0]
                set_mode = [5,13]
                step = 4

        if lost_tracking:
            motor_speeds = [150,-170]
        else:
            if abs(angle_difference) > 5:
                if (angle_difference > 0):
                    motor_speeds = [150,-150]
                if (angle_difference < 0):
                    motor_speeds = [-150,150]
            else:
                motor_speeds = [150,150]

    data = {}
    data["m"] = motor_speeds
    data["s"] = set_mode
    data = json.dumps(data)
    if ser.isOpen():
        ser.write(data.encode('ascii'))
        ser.flush()

    if step == 4:
        ser.close()
        step = 0

    time.sleep(0.2)



    # Display the resulting frame
    cv2.imshow("frame - Press 'q' to close",frame_markers)
    #cv2.imshow('frame',gray)

    key = cv2.waitKey(1) & 0xFF
    #if key == ord('s'):
    #    cv2.imwrite('./Images/photo{}.jpg'.format(i),frame_markers)
    #    i+=1
    if key == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()