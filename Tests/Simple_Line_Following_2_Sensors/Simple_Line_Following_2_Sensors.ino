#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <HCSR04.h>
#include <Servo.h>

Servo myservo;

Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *motorL = AFMS.getMotor(1);
Adafruit_DCMotor *motorR = AFMS.getMotor(2);

const int lineSensorL = A0;
const int lineSensorR = A1;

bool left;
bool right;

int motorLspeed;
int motorRspeed;

int triggerPin = 2;
int echoPin = 3;
UltraSonicDistanceSensor distanceSensor(triggerPin, echoPin);

bool over_line(int sensor){
  if (analogRead(sensor) > 400){
    return true;
  }
  else{
    return false;
  }
}

void setup() {
  // put your setup code here, to run once:

  AFMS.begin();

  pinMode (A0, INPUT);
  pinMode (A1, INPUT);

  Serial.begin(9600);
  
  double distance = distanceSensor.measureDistanceCm();


  list<double> distance_list;

  myservo.attach(9);
  myservo.write(90);

}

void loop() {
  // put your main code here, to run repeatedly:

    delay(100);

  motorL->run(RELEASE);
  motorR->run(RELEASE);

  left = over_line(lineSensorL);
  right = over_line(lineSensorR);


  if (distance_list.size()<2 and distance >= 0){
    distance_list.push_back(distance);
  }

  
  else if (distance_list.size() == 2 and distance >= 0){
    
    distance_list.push_back(distance);
    Serial.println(distance_list);
    
    float sum = 0;
    for (int i = 0; i < 3; i++){
      sum += distance_list[i];
    }
    float average_dist = sum/3;

    
    distance_list.remove(distance_list[0]);

  }
  if (!left){
    motorLspeed = 200;
  }
  else{
    motorLspeed = -50;
  }

  if (!right){
    motorRspeed = 200;
  }
  else{
    motorRspeed = -50;
  }
  if (average_dist <7){
    motorLspeed = 0;
    motorRspeed = 0;
  }


  if (motorLspeed > 0) {
      motorL->run(FORWARD);
  }
  else if(motorLspeed == 0){
    motorL->run(RELEASE);
  }
  else motorL->run(BACKWARD);
  
  if (motorRspeed > 0) {
      motorR->run(FORWARD);
  }
  else if(motorRspeed == 0){
    motorR->run(RELEASE);
  }
  else motorR->run(BACKWARD);

  

  motorL->setSpeed(abs(motorLspeed));
  motorR->setSpeed(abs(motorRspeed));

  
}
