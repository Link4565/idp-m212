#include "Sensors.h"

void read_sensors(void) {

	//Read the line sensors
	lineL = digitalRead(lineSensorL);
	lineC = digitalRead(lineSensorC);
	lineR = digitalRead(lineSensorR);

	/*
	ultrasoundDistance = distanceSensor.measureDistanceCm();
	if (ultrasoundDistance != -1) {
		ultrasoundReadings[3] = ultrasoundReadings[2];
		ultrasoundReadings[2] = ultrasoundReadings[1];
		ultrasoundReadings[1] = ultrasoundReadings[0];
		ultrasoundReadings[0] = ultrasoundDistance;
	}

	ultrasoundDistance = (ultrasoundReadings[0] + ultrasoundReadings[1] + ultrasoundReadings[2] + ultrasoundReadings[3]) / 4;
	*/

	//Read the distance sensor
	ultrasoundDistance = distanceSensor.measureDistanceCm();
	//Ignore invalid readings
	if (ultrasoundDistance != -1) {
		//Record the last 10 readings
		for (byte i = 9; i > 0; i = i - 1) {
			ultrasoundReadings[i] = ultrasoundReadings[i - 1];
		}
		ultrasoundReadings[0] = ultrasoundDistance;
	}
	//Average the last 10 readings
	ultrasoundDistance = ultrasoundReadings[0];
	for (byte i = 0; i < 10; i = i + 1) {
		ultrasoundDistance += ultrasoundReadings[i];
	}
	ultrasoundDistance = ultrasoundDistance / 10;

	//Read the wheel encoder state
	prev_encoderL = encoderL;
	prev_encoderR = encoderR;

	encoderL = digitalRead(interrupterL);
	encoderR = digitalRead(interrupterR);

	//Calculate the previous position and orientation of the robot
	double leftWheelX = ((-robotWidth / 2) * cos(direction)) + xPos;
	double leftWheelY = ((-robotWidth / 2) * sin(direction)) + yPos;
	double rightWheelX = ((-robotWidth / 2) * cos(direction)) + xPos;
	double rightWheelY = ((-robotWidth / 2) * sin(direction)) + yPos;
	double theta = (wheelCircumference / encoderDivisions) / robotWidth;

	//Calculate the new position and orientation of the robot
	if (encoderL != prev_encoderL) {
		//If travelling straight forwards
		if (encoderR != prev_encoderR) {
			if (motorLspeed = motorRspeed) {
				//Move the position forwards
				xPos += (wheelCircumference / encoderDivisions) * cos(direction) * (motorLspeed / abs(motorLspeed));
				yPos += (wheelCircumference / encoderDivisions) * sin(direction) * (motorLspeed / abs(motorLspeed));
			}
		}
		else {
			//Change the position and orientation of the robot
			if (motorLspeed > 0) {
				theta = -theta;
			}
			direction += theta;
			xPos = -cos(theta) * rightWheelX + sin(theta) * rightWheelY + rightWheelX;
			yPos = -sin(theta) * rightWheelX - cos(theta) * rightWheelY + rightWheelY;
		}
	}
	else if (encoderR != prev_encoderR) {
		//Change the position and orientation of the robot
		if (motorRspeed < 0) {
			theta = -theta;
		}
		direction += theta;
		xPos = -cos(theta) * leftWheelX + sin(theta) * leftWheelY + leftWheelX;
		yPos = -sin(theta) * leftWheelX - cos(theta) * leftWheelY + leftWheelY;
	}
	/*
	double redness;
	redness = getRedness();

	if (redness > 0.3) { colour = 0; }
	else if (redness < -0.3) { colour = 1; }
	*/

	//Read the current colour sensor values
	double redness;
	redness = getRedness();

	//Record the last 20 colour sensor readings
	for (byte i = 19; i > 0; i = i - 1) {
		colourReadings[i] = colourReadings[i - 1];
	}
	colourReadings[0] = redness;

	//Test for 3 of the last 20 readings being blue
	colour = 0;
	for (byte i = 0; i < 20; i = i + 1) {
		if (redness < 0.1) colour += 1;
	}
	//Decide blue if 3 of the last 20 readings were blue
	if (colour > 3) colour = 1;

}


float getRedness() {

	//Calculate a 'redness' value based on the intensities of the red and blue sensors

	float R = 0.2 + intensity(analogRead(A1));
	float B = 0.2 + 5 * intensity(analogRead(A0));

	return (R - B) / (R + B);

}

float intensity(int v) {
	return (1023.0 / float(v) - 1) / 0.1; //Convert voltage to light intensity
}

void display_leds(void) {
	//Change the yellow led state if 0.25s have passed
	if (millis()-ledTimer > 250) {
		yellowLEDstate = !yellowLEDstate;
		ledTimer = millis();
	}
	//Turn on/off the yellow, red and blue leds
	digitalWrite(yellowLED, yellowLEDstate);
	digitalWrite(blueLED, carryingBlue);
	digitalWrite(redLED, carryingRed);
}