#include "Config.h"

//Declare all global variables


const int lineSensorL = 2;
const int lineSensorC = 3;
const int lineSensorR = 4;

const int colourSensorR = A0 ;
const int colourSensorB = A1 ;

const int interrupterL = 5;
const int interrupterR = 6;

const int yellowLED = 11;
const int blueLED = 12;
const int redLED = 13;

const double robotWidth = 20;
const double wheelCircumference = 10;
const int encoderDivisions = 35; 

const int servoHandMaxAngle = 125;
const int servoWristMaxAngle = 90;
const int servoHandMinAngle = 70;
const int servoWristMinAngle = 35;

const double ultrasoundThreshold = 7.5;
 
short int motorLspeed = 0;
short int motorRspeed = 0;
short int Prev_motorLspeed = 0;
short int Prev_motorRspeed = 0;

double ultrasoundReadings[10] {500};
double ultrasoundDistance = 0;
bool lineL = 0;
bool lineC = 0;
bool lineR = 0;
bool encoderL = 0;
bool encoderR = 0;
bool prev_encoderL = 0;
bool prev_encoderR = 0;

//Red = 0 Blue = 1
int colour = 0;
double colourReadings[20] {};

bool error = 0;

double xPos = 0;
double yPos = 0;
double direction = PI/2; //in rad ccw from +ve x
double last_direction = PI/2;

short int servoHandAngle = servoHandMaxAngle;
short int servoWristAngle = servoWristMaxAngle;
bool servoHandDirection = 1; //true open
bool servoWristDirection = 1; // true up

double servoTimer = 0;
bool servoPosition = 0; //true grabbed

int mode = 0;
int submode = 0;

const double xPos_T_junc = 1.0;
const double yPos_T_junc = 5.0;
const double xPos_intial_junc = 0.5;
const double yPos_intial_junc = 5.0;
const double xPos_blue_targets_junc = 0.1;
const double yPos_blue_targets_junc = 10;

const double xPos_blue_top_target = 0;
const double yPos_blue_top_target = 12;
const double xPos_blue_btm_target = 12;
const double yPos_blue_btm_target = 12;
const double xPos_red_right_target = 15;
const double yPos_red_right_target = 4;
const double xPos_red_btm_target = 3;
const double yPos_red_btm_target = -2;

int red_packages_aside = 0;
int blue_done = 0;
int red_done = 0;

double ledTimer = 0;
bool yellowLEDstate = 0;

double decisionTimer = 0;

bool carryingRed = 0;
bool carryingBlue = 0;

bool overTarget = 0;
int targets_crossed = 0;

bool controlledByPython = false;


void init_vars(void) {
	motorLspeed = 0;
	motorRspeed = 0;
	Prev_motorLspeed = 0;
	Prev_motorRspeed = 0;

	//ultrasoundDistance = 0;
	lineL = 0;
	lineC = 0;
	lineR = 0;
	encoderL = 0;
	encoderR = 0;

	colour = 0;

	error = 0;

	carryingRed = 0;
	carryingBlue = 0;

	blue_done = 0;
	red_done = 0;

	decisionTimer = 0;

	overTarget = 0;
	targets_crossed = 0;

	controlledByPython = false;

}
