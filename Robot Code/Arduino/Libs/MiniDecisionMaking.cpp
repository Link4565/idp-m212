#include "MiniDecisionMaking.h"


void minidecisionmaking(void) {
	if (mode == 0) {
		if (submode == 0) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 17;
				return;
			}
		}
	}

	if (mode == 1) {
		if (lineC && lineR && lineL) {
			if (submode == 1) {
				mode = 2;
				submode = 1;
				return;
			}

			if (submode == 2) {
				mode = 2;
				submode = 2;
				return;
			}

			if (submode == 3) {
				mode = 6;
				submode = 1;
				return;
			}

			if (submode == 4) {
				mode = 6;
				submode = 2;
				return;
			}
		}
	}

	if (mode == 2) {
		if (submode == 1) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 1;
				submode = 2;
				return;
			}
		}

		if (submode == 2) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 3;
				submode = 1;
				return;
			}
		}
	}

	if (mode == 3) {
		if (submode == 1) {
			if (ultrasoundDistance <= ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 1;
				return;
			}

			if (ultrasoundDistance <= ultrasoundThreshold && colour == 1) {
				mode = 4;
				submode = 2;
				return;
			}
		}
		
	}

	if (mode == 4) {
		if (submode == 1) {
			if (servoPosition ==  1) {
				mode = 12;
				submode = 1;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 2) {
			if (servoPosition == 1) {
				mode = 12;
				submode = 1;
				carryingBlue = 1;
				return;
			}
		}
	}

	if (mode == 5) {
		if (submode == 1) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				if (lineC) {
					decisionTimer = 0;
					mode = 1;
					submode = 3;
					return;
				}
			}
		}
	}

	if (mode == 6) {
		if (submode == 1) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 5000) {
				decisionTimer = 0;
				mode = 1;
				submode = 4;
				return;
			}
		}

		if (submode == 2) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 5000) {
				decisionTimer = 0;
				mode = 13;
				submode = 1;
				return;
			}
		}
	}

	if (mode == 7) {
		if (submode == 1) {
			if (servoPosition == 0) {
				mode = 5;
				submode = 1;
				carryingBlue = 0;
				carryingRed = 0;
				return;
			}

		}
	}

	if (mode == 12) {
		if (submode == 1) {
			if (lineC && lineL && lineR) {
				mode = 14;
				submode = 1;
				return;
			}
		}
	}

	if (mode == 13) {
		if (submode == 1) {
			if (lineC && lineR && lineL) {
				mode = 100;
				submode = 100;
				return;
			}
		}
	}

	if (mode == 14) {
		if (submode == 1) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 7;
				submode = 1;
				return;
			}
		}
	}

	if (mode == 100) {
		if (submode == 100) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 5000) {
				decisionTimer = 0;
				mode = 101;
				submode = 101;
				return;
			}
		}
	}

	if (mode == 101) {
		if (submode == 101 ) {
			return;
		}
	}
}