#include "DecisionMaking.h"


void decisionmaking(void) {
	if (mode == 0) {
		//Leave the start area
		if (submode == 0) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 1;
				return;
			}
		}
	}
	
	
	if (mode == 1) {
		//line following(expecting a junction) 
		if (submode == 1) {
			if (abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5) {
				//within T junction
				mode = 2;
				last_direction = direction;
				submode = 1;
				return;
			}
		}

		if (submode == 2) {
			if (abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5) {
				//within T junction
				mode = 2;
				last_direction = direction;
				submode = 3;
				return;
			}
		}

		if (submode == 3) {
			if (abs(xPos - xPos_intial_junc) <= 0.5 && abs(yPos - yPos_intial_junc) <= 0.5) {
				//within initial junction
				mode = 6;
				last_direction = direction;
				submode = 1;
				return;
			}
		}

		if (submode == 4) {
			if(abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5){
				//within T junction
				mode = 2;
				last_direction = direction;
				submode = 4;
				return;
			}
		}

		if (submode == 5) {
			if (abs(xPos - xPos_intial_junc) <= 0.5 && abs(yPos - yPos_intial_junc) <= 0.5) {
				//within initial junction
				mode = 6;
				last_direction = direction;
				submode = 3;
				return;
			}
		}

		if (submode == 6) {
			if (abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5) {
				//within T junction
				mode = 6;
				last_direction = direction;
				submode = 4;
				return;
			}
		}

		if (submode == 7) {
			if (abs(xPos - xPos_intial_junc) <= 0.5 && abs(yPos - yPos_intial_junc) <= 0.5) {
				//within initial junction
				mode = 2;
				last_direction = direction;
				submode = 5;
				return;
			}
		}

		if (submode == 8) {
			if (abs(xPos - xPos_blue_targets_junc) <= 0.5 && abs(yPos - yPos_blue_targets_junc) <= 0.5) {
				//within blue target junction
				mode = 2;
				last_direction = direction;
				submode = 6;
				return;
			}
		}

		if (submode == 9) {
			if (abs(xPos - xPos_blue_targets_junc) <= 0.5 && abs(yPos - yPos_blue_targets_junc) <= 0.5) {
				//within blue target junction
				mode = 6;
				last_direction = direction;
				submode = 5;
				return;
			}
		}

		if (submode == 10) {
			if (abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5) {
				//within T junction
				mode = 2;
				last_direction = direction;
				submode = 7;
				return;
			}
		}

		if (submode == 11) {
			if (abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5) {
				//within T junction
				mode = 6;
				last_direction = direction;
				submode = 6;
				return;
			}
		}

		if (submode == 12) {
			if (abs(xPos - xPos_intial_junc) <= 0.5 && abs(yPos - yPos_intial_junc) <= 0.5) {
				//within initial junction
				mode = 2;
				last_direction = direction;
				submode = 8;
				return;
			}
		}

		if (submode == 13) {
			if (abs(xPos - xPos_blue_targets_junc) <= 0.5 && abs(yPos - yPos_blue_targets_junc) <= 0.5) {
				//within blue target junction
				mode = 6;
				last_direction = direction;
				submode = 7;
				return;
			}
		}

		if (submode == 14) {
			if (abs(xPos - xPos_blue_targets_junc) <= 0.5 && abs(yPos - yPos_blue_targets_junc) <= 0.5) {
				//within blue target junction
				mode = 2;
				last_direction = direction;
				submode = 9;
				return;
			}
		}

		if (submode == 15) {
			if (red_packages_aside < 2 && abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5) {
				//within T junction
				mode = 2;
				last_direction = direction;
				submode = 2;
				return;
			}

			if (red_packages_aside == 2 && abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5) {
				//within T junction
				mode = 6;
				last_direction = direction;
				submode = 2;
				return;
			}
		}

		if (submode == 16) {
			if (abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5 && ultrasoundDistance <= 20) {
				//within T junction
				mode = 8;
				submode = 1;
				return;
			}
			if(abs(xPos - xPos_T_junc) <= 0.5 && abs(yPos - yPos_T_junc) <= 0.5 && ultrasoundDistance > 20){
				//within T junction
				mode = 10;
				submode = 1;
				return;
			}
		}

		if (submode == 17) {
			if (abs(xPos - xPos_intial_junc) <= 0.5 && abs(yPos - yPos_intial_junc) <= 0.5) {
				mode = 2;
				last_direction = direction;
				submode = 10;
				return;
			}
		}

		if (submode == 18) {
			if (abs(xPos - xPos_intial_junc) <= 0.5 && abs(yPos - yPos_intial_junc) <= 0.5) {
				mode = 6;
				last_direction = direction;
				submode = 8;
				return;
			}
		}

		if (submode == 19) {
			if (abs(xPos - xPos_intial_junc) <= 0.5 && abs(yPos - yPos_intial_junc) <= 0.5) {
				mode = 6;
				last_direction = direction;
				submode = 9;
				return;
			}
		}
	}

	if (mode == 2) {
		//Turn right on junction
		if ( ( abs(last_direction - direction) >= (PI / 3) && lineC )) {
			if (submode == 1) {
				mode = 3;
				submode = 1;
				return;
			}

			if (submode == 2) {
				mode = 3;
				submode = 5;
				return;
			}

			if (submode == 3) {
				mode = 1;
				submode = 3;
				return;
			}

			if (submode == 4) {
				mode = 1;
				submode = 5;
				return;
			}

			if (submode == 5) {
				mode = 1;
				submode = 8;
				return;
			}

			if (submode == 6) {
				mode = 11;
				submode = 1;
				return;
			}

			if (submode == 7) {
				mode = 3;
				submode = 3;
				return;
				
			}

			if (submode == 8) {
				mode = 1;
				submode = 13;
				return;
			}

			if (submode == 9) {
				mode = 1;
				submode = 19;
				return;
			}

			if (submode == 10) {
				mode = 1;
				submode = 1;
				return;
			}
		}
	}

	if (mode == 3) {
		//Line following(expexting a package)
		if (submode == 1) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 1;
				return;
			}
			if (ultrasoundDistance < ultrasoundThreshold && colour == 1) {
				mode = 4;
				submode = 2;
				return;
			}
		}

		if (submode == 2) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 1;
				return;
			}
			if (ultrasoundDistance < ultrasoundThreshold && colour == 1) {
				mode = 4;
				submode = 2;
				return;
			}
		}

		if (submode == 3) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 3;
				return;
			}
			if (ultrasoundDistance < ultrasoundThreshold && colour == 1) {
				mode = 4;
				submode = 4;
				return;
			}
		}

		if (submode == 4) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 3;
				return;
			}
			if (ultrasoundDistance < ultrasoundThreshold && colour == 1) {
				mode = 4;
				submode = 4;
				return;
			}
		}

		if (submode == 5) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 5;
				return;
			}
		}

		if (submode == 6) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 6;
				return;
			}
		}

		if (submode == 7) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 7;
				return;
			}
		}

		if (submode == 8) {
			if (ultrasoundDistance < ultrasoundThreshold && colour == 0) {
				mode = 4;
				submode = 8;
				return;
			}
		}
	}

	if (mode == 4) {
		//Initiate grabbing an carrying
		if (submode == 1) {
			if (servoPosition == 1) {
				mode = 1;
				submode = 16;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 2) {
			if (servoPosition == 1) {
				mode = 5;
				last_direction = direction;
				submode = 4;
				carryingBlue = 1;
				return;
			}
		}

		if (submode ==3) {
			if (servoPosition == 1) {
				mode = 1;
				submode = 16;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 4) {
			if (servoPosition == 1) {
				mode = 5;
				last_direction = direction;
				submode = 6;
				carryingBlue = 1;
				return;
			}
		}

		if (submode == 5) {
			if (servoPosition == 1) {
				mode = 5;
				last_direction = direction;
				submode = 1;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 6) {
			if (servoPosition == 1) {
				mode = 5;
				last_direction = direction;
				submode = 5;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 7) {
			if (servoPosition == 1) {
				mode = 5;
				last_direction = direction;
				submode = 3;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 8) {
			if (servoPosition == 1) {
				mode = 12;
				submode = 2;
				carryingRed = 1;
				return;
			}
		}
	}

	if (mode == 5) {
		//turn around
		if ((abs(last_direction - direction) > (PI/2) && lineC) || abs(last_direction - direction) > ( 3*PI/2) ) {
			if (submode == 1) {
				mode = 12;
				submode = 1;
				return;
			}

			if (submode == 2) {
				mode = 12;
				submode = 2;
				return;
			}


			if (submode == 3) {
				mode = 12;
				submode = 1;
				return;
			}


			if (submode == 4) {
				mode = 1;
				submode = 6;
				return;
			}


			if (submode == 5) {
				mode = 1;
				submode = 9;
				return;
			}


			if (submode == 6) {
				mode = 1;
				submode = 11;
				return;
			}


			if (submode == 7) {
				mode = 1;
				submode = 14;
				return;
			}


			if (submode == 8) {
				if (red_packages_aside < 2) {
					mode = 3;
					submode = 6;
					return;
				}
				if (red_packages_aside == 2) {
					mode = 3;
					submode = 8;
					return;
				}

			}

			if (submode == 9) {
				if (red_packages_aside < 2) {
					mode = 1;
					submode = 2;
					return;
				}
				if (red_packages_aside == 2) {
					mode = 1;
					submode = 4;
					return;
				}

			}

			if (submode == 10) {
				if (blue_done == 0) {
					mode = 3;
					submode = 2;
					return;
				}

				if (blue_done == 1) {
					mode = 3;
					submode = 4;
					return;
				}
			}

			if (submode == 11) {
				if (blue_done == 0) {
					mode = 3;
					submode = 2;
					return;
				}

				if (blue_done == 1) {
					mode = 3;
					submode = 4;
					return;
				}
			}
		}
	}

	if (mode == 6) {
		//Turn left on junction
		if (abs(last_direction - direction) >= PI / 2 && lineC) {
			if (submode == 1) {
				mode = 13;
				submode = 1;
				return;
			}

			if (submode == 2) {
				mode = 3;
				submode = 7;
				return;
			}

			if (submode == 3) {
				mode = 13;
				submode = 2;
				return;
			}

			if (submode == 4) {
				mode = 1;
				submode = 7;
				return;
			}

			if (submode == 5) {
				mode = 1;
				submode = 18;
				return;
			}

			if (submode == 6) {
				mode = 1;
				submode = 12;
				return;
			}

			if (submode == 7) {
				mode = 11;
				submode = 1;
				return;
			}

			if (submode == 8) {
				mode = 1;
				submode = 10;
				return;
			}

			if (submode == 9) {
				mode = 1;
				submode = 15;
				return;
			}
		}

	}

	if (mode == 7) {
		//Readjust grabber position, and place package down
		if (servoPosition == 0) {
			if (submode == 1) {
				mode = 5;
				last_direction = direction;
				submode = 5;
				carryingBlue = 0;
				return;
			}

			if (submode == 2) {
				mode = 5;
				last_direction = direction;
				submode = 7;
				carryingBlue = 0;
				return;
			}

			if (submode == 3) {
				mode = 5;
				last_direction = direction;
				submode = 8;
				carryingRed = 0;
				return;
			}

			if (submode == 4) {
				mode = 5;
				last_direction = direction;
				submode = 9;
				carryingRed = 0;
				return;
			}
		}
	}

	if (mode == 8) {
		//Push packages ahead for 20 cm, reverse for 10cm and stop
		if (submode == 1) {
			/*if () {
				mode = 14;
				submode = 1;
			}*/
		}
	}

	if (mode == 9) {
		//Place red packages down on the line
		if (servoPosition == 0) {
			carryingRed = 0;
			if (submode == 1) {
				mode = 5;
				last_direction = direction;
				submode = 10;
				return;
			}

			if (submode == 2) {
				mode = 5;
				last_direction = direction;
				submode = 11;
				return;
			}
		}
	}

	if (mode == 10) {
		//Stop 20cm beyond junction
		if (motorLspeed == 0 && motorRspeed == 0) {
			if (submode == 1) {
				mode = 9;
				submode = 2;
				return;
			}
		}
	}

	if (mode == 11) {
		//Line following (expecting blue target)

		if (submode == 1) {
			if (lineC && lineL && lineR && abs(xPos - xPos_blue_top_target) < 0.5 && abs(yPos - yPos_blue_top_target ) < 0.5) {
				//complete blue top
				mode = 7;
				submode = 1;
				return;
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR && abs(xPos - xPos_blue_btm_target) < 0.5 && abs(yPos - yPos_blue_btm_target) < 0.5) {
				//complete blue btm
				mode = 7;
				submode = 2;
				return;
			}	
		}
	}

	if (mode == 12) {
		//Line following (expecting red target)

		if (submode == 1) {
			if (lineC && lineL && lineR && abs(xPos - xPos_red_btm_target) < 0.5 && abs(yPos - yPos_red_btm_target) < 0.5) {
				//complete red btm
				mode = 7;
				submode = 3;
				return;
			}	
		}

		if (submode == 2) {
			if (lineC && lineL && lineR && abs(xPos - xPos_red_right_target) < 0.5 && abs(yPos - yPos_red_right_target) < 0.5) {
				//complete red right
				mode = 7;
				submode = 4;
				return;
			}
		}
	}

	if (mode == 13) {
		//Line following (expecting the starting area)
		if (xPos < 0.5 && yPos < 0.5 && lineC && lineL && lineR) {
			if (submode == 1 || submode == 2) {
				mode = 100;
				submode = 100;
				return;
			}
		}
	}

	if (mode == 14) {
		//Reverse for 10 cm and stop
		/*if (submode == 1) {
			if () {
				return;
			}
		}*/
	}

	if (mode == 100) {
		if (submode == 100) {
			//enter the start area
			if (lineC && lineR && lineL) {
				mode = 101;
				submode = 101;
				return;
			}
		}
	}

	if (mode == 101) {
		if (submode == 101) {
			//Stop at the start area

			return;
		}
	}

}