#include "Grabbing.h"



void move_wrist(void) {
	//Open / Close wrist servo one degree each time the function is run
	if (servoWristDirection) {
		if (servoWristAngle < 180){
			wrist.write(servoWristAngle); 
			//wrist.write(180-servoWristAngle); //Use if servo direction needs to be reversed
			servoWristAngle += 1;
		}
	}
	else {
		if (servoWristAngle > 0){
			wrist.write(servoWristAngle); 
			//wrist.write(180-servoWristAngle);
			servoWristAngle -= 1;
		}
	}
}

void move_hand(void) {
	//Open / Close hand servo one degree each time the function is run
	if (servoHandDirection) {
		if (servoHandAngle < 180) {
			hand.write(servoHandAngle); //hand.write(180-servoHandAngle);
			servoHandAngle += 1;
		}
	}
	else {
		if (servoHandAngle > 0) {
			hand.write(servoHandAngle); //hand.write(180-servoHandAngle);
			servoHandAngle -= 1;
		}
	}
}

void grab(void) {
	//Lower the wrist
	if (servoWristAngle > servoWristMinAngle && servoHandAngle > servoHandMinAngle) {
		servoWristDirection = 0;
		servoHandDirection = 0;
		move_wrist();
	}
	//Close the hand
	if (servoWristAngle == servoWristMinAngle && servoHandAngle > servoHandMinAngle) {
		if (servoTimer == 0) {
			servoTimer == millis();
		}
		if (millis() - servoTimer > 1000) {
			move_hand();
		}
	}
	//Change the wrist direction
	if (servoWristAngle == servoWristMinAngle && servoHandAngle == servoHandMinAngle) {
		servoTimer = 0;
		servoWristDirection = 1;
		servoWristAngle += 1;
	}
	//Raise the wrist
	if (servoWristAngle > servoWristMinAngle && servoHandAngle == servoHandMinAngle) {
		if (servoTimer == 0) {
			servoTimer == millis();
		}
		if (millis() - servoTimer > 1000) {
			move_wrist();
		}
	}
	//Flag that the robot has grabbed the block
	if (servoWristAngle == servoWristMaxAngle && servoHandAngle == servoHandMinAngle) {
		servoTimer = 0;
		servoPosition = 1;
	}
}

void drop(void) {
	//Drop the wrist
	if (servoWristAngle > servoWristMinAngle && servoHandAngle == servoHandMinAngle) {
		servoWristDirection = 0;
		servoHandDirection = 1;
		move_wrist();
	}
	//Open the hand
	if (servoWristAngle == servoWristMinAngle && servoHandAngle < servoHandMaxAngle) {
		if (servoTimer == 0) {
			servoTimer == millis();
		}
		if (millis() - servoTimer > 1000) {
			move_hand();
		}
	}
	//Change the wrist direction
	if (servoWristAngle == servoWristMinAngle && servoHandAngle == servoHandMaxAngle) {
		servoTimer = 0;
		servoWristDirection = 1;
		servoWristAngle += 1;
	}
	//Raise the wrist
	if (servoWristAngle > servoWristMinAngle && servoHandAngle == servoHandMaxAngle) {
		if (servoTimer == 0) {
			servoTimer == millis();
		}
		if (millis() - servoTimer > 1000) {
			move_wrist();
		}
	}
	//Flag that the block has been dropped
	if (servoWristAngle == servoWristMaxAngle && servoHandAngle == servoHandMaxAngle) {
		servoTimer = 0;
		servoPosition = 0;
	}
}

void centre_block_down(void) {
	//Lower the wrist
	if (servoWristAngle > servoWristMinAngle && servoHandAngle > servoHandMinAngle) {
		servoWristDirection = 0;
		servoHandDirection = 0;
		move_wrist();
	}
	//Close the hand
	if (servoWristAngle == servoWristMinAngle && servoHandAngle > servoHandMinAngle) {
		if (servoTimer == 0) {
			servoTimer == millis();
		}
		if (millis() - servoTimer > 1000) {
			move_hand();
		}
	}
	//Flag that the arms are down
	if (servoWristAngle == servoWristMinAngle && servoHandAngle == servoHandMinAngle) {
		servoTimer = 0;
		servoPosition = 1;
	}
}

void centre_block_up(void) {
	//Open the hand
	if (servoWristAngle == servoWristMinAngle && servoHandAngle < servoHandMaxAngle) {
		servoWristDirection = 0;
		servoHandDirection = 1;
		if (servoTimer == 0) {
			servoTimer == millis();
		}
		if (millis() - servoTimer > 1000) {
			move_hand();
		}
	}
	//Change the wrist direction
	if (servoWristAngle == servoWristMinAngle && servoHandAngle == servoHandMaxAngle) {
		servoTimer = 0;
		servoWristDirection = 1;
		servoWristAngle += 1;
	}
	//Raise the wrist
	if (servoWristAngle > servoWristMinAngle && servoHandAngle == servoHandMaxAngle) {
		if (servoTimer == 0) {
			servoTimer == millis();
		}
		if (millis() - servoTimer > 1000) {
			move_wrist();
		}
	}
	//Flag that the arms are up
	if (servoWristAngle == servoWristMaxAngle && servoHandAngle == servoHandMaxAngle) {
		servoTimer = 0;
		servoPosition = 0;
	}
}