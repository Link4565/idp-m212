#include "DecisionMakingSideShortBlue.h"

void decisionmakingsideshortblue(void) {
	if (mode == 0) {
		//Leave the start area
		if (submode == 0) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 21;
				submode = 17;
				return;
			}
		}
	}


	if (mode == 1) {
		/*if (submode == 6 || submode == 11 || submode == 16) {
			if (overTarget) {
				if (!lineL && !lineR) {
					overTarget = 0;
					targets_crossed -= 1;
				}
			}
			else {
				if (lineL && lineC && lineR) {
					overTarget = 1;
				}
			}
		}*/
		//line following(expecting a junction) 
		if (submode == 1) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 1;
				return;
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 3;
				return;
			}
		}

		if (submode == 3) {
			if (lineC && lineL && lineR) {
				//within initial junction
				mode = 6;
				//last_direction = direction;
				submode = 1;
				return;
			}
		}

		if (submode == 4) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 4;
				return;
			}
		}

		if (submode == 5) {
			if (lineC && lineL && lineR) {
				//within initial junction
				mode = 6;
				//last_direction = direction;
				submode = 3;
				return;
			}
		}

		if (submode == 6) {
			if (lineC && lineL && lineR) {
				if (targets_crossed == 0) {
					//within T junction
					mode = 6;
					//last_direction = direction;
					submode = 4;
					return;
				}

				if (targets_crossed > 0) {
					mode = 19;
					submode = 1;
					return;
				}
			}

			
		}

		if (submode == 7) {
			if (lineC && lineL && lineR) {
				//within initial junction
				mode = 2;
				//last_direction = direction;
				submode = 5;
				return;
			}
		}

		if (submode == 8) {
			if (lineC && lineL && lineR) {
				//within blue target junction
				mode = 6;
				//last_direction = direction;
				submode = 12;
				return;
			}
		}

		if (submode == 9) {
			if (lineC && lineL && lineR) {
				//within blue target junction
				mode = 6;
				//last_direction = direction;
				submode = 5;
				return;
			}
		}

		if (submode == 10) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 7;
				return;
			}
		}

		if (submode == 11) {
			if (lineC && lineL && lineR) {
				if (targets_crossed == 0) {
					//within T junction
					mode = 6;
					//last_direction = direction;
					submode = 6;
					return;
				}

				if (targets_crossed > 0) {
					mode = 19;
					submode = 2;
					return;
				}
			}
		}

		if (submode == 12) {
			if (lineC && lineL && lineR) {
				//within initial junction
				mode = 2;
				//last_direction = direction;
				submode = 8;
				return;
			}
		}

		if (submode == 13) {
			if (lineC && lineL && lineR) {
				//within blue target junction
				mode = 6;
				//last_direction = direction;
				submode = 7;
				return;
			}
		}

		if (submode == 14) {
			if (lineC && lineL && lineR) {
				//within blue target junction
				mode = 2;
				//last_direction = direction;
				submode = 9;
				return;
			}
		}

		if (submode == 15) {
			if (red_packages_aside < 2 && lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 2;
				return;
			}

			if (red_packages_aside == 2 && lineC && lineL && lineR) {
				//within T junction
				mode = 6;
				//last_direction = direction;
				submode = 2;
				return;
			}
		}

		if (submode == 16) {
			if (lineC && lineL && lineR) {
				if (targets_crossed == 0) {
					//within T junction
					mode = 2;
					//last_direction = direction;
					submode = 11;
					return;
				}

				if (targets_crossed > 0) {
					mode = 19;
					submode = 4;
					return;
				}
			}
		}

		if (submode == 17) {
			if (lineC && lineL && lineR) {
				mode = 2;
				//last_direction = direction;
				submode = 10;
				return;
			}
		}

		if (submode == 18) {
			if (lineC && lineL && lineR) {
				mode = 6;
				//last_direction = direction;
				submode = 8;
				return;
			}
		}

		if (submode == 19) {
			if (lineC && lineL && lineR) {
				mode = 2;
				//last_direction = direction;
				submode = 15;
				return;
			}
		}
		if (submode == 20) {
			if (lineC && lineL && lineR) {
				mode = 6;
				//last_direction = direction;
				submode = 10;
				return;
			}
		}
		if (submode == 21) {
			if (lineC && lineL && lineR) {
				mode = 6;
				//last_direction = direction;
				submode = 10;
				return;
			}
		}
		if (submode == 22) {
			if (lineC && lineL && lineR) {
				mode = 6;
				//last_direction = direction;
				submode = 11;
				return;
			}
		}
		if (submode == 23) {
			if (lineC && lineL && lineR) {
				mode = 2;
				//last_direction = direction;
				submode = 12;
				return;
			}
		}
		if (submode == 24) {
			if (!lineL && lineC && !lineR) {
				mode = 23;
				submode = 1;
				return;
			}
		}
		if (submode == 25) {
			if (!lineL && lineC && !lineR) {
				mode = 23;
				submode = 3;
				return;
			}
		}
		if (submode == 26) {
			if (!lineL && lineC && !lineR) {
				mode = 23;
				submode = 5;
				return;
			}
		}
		if (submode == 27) {
			if (!lineL && lineC && !lineR) {
				if (targets_crossed == 0) {
					mode = 21;
					submode = 6;
					return;
				}
				else {
					mode = 22;
					submode = 6;
					return;
				}
			}
		}
		if (submode == 28) {
			if (!lineL && lineC && !lineR) {
				if (targets_crossed == 0) {
					mode = 21;
					submode = 11;
					return;
				}
				else {
					mode = 22;
					submode = 11;
					return;
				}
			}
		}
		if (submode == 29) {
			if (!lineL && lineC && !lineR) {
				if (targets_crossed < 2) {
					mode = 21;
					submode = 1;
					return;
				}
				else {
					mode = 22;
					submode = 1;
					return;
				}
			}
		}
		if (submode == 30) {
			if (!lineL && lineC && !lineR) {
				if (targets_crossed == 0) {
					mode = 21;
					submode = 16;
					return;
				}
				else {
					mode = 22;
					submode = 16;
					return;
				}
			}
		}
		if (submode == 31) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			if (millis() - decisionTimer > 4000) {
				if (ultrasoundDistance <= 20) {
					decisionTimer = 0;
					//within T junction
					mode = 14;
					submode = 1;
					return;
				}
				if (ultrasoundDistance > 20) {
					decisionTimer = 0;
					//within T junction
					mode = 9;
					submode = 2;
					return;
				}
			}
		}
	}

	if (mode == 2) {
		//Turn right on junction
		if (submode == 1) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 23;
				submode = 1;
				return;
			}
		}

		if (submode == 2) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 23;
				submode = 5;
				return;
			}
		}

		if (submode == 3) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 3;
				return;
			}
		}

		if (submode == 4) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 5;
				return;
			}
		}

		if (submode == 5) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 8;
				return;
			}
		}

		if (submode == 6) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 6;
				submode = 12;
				return;
			}
		}

		if (submode == 7) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 23;
				submode = 3;
				return;
			}
		}

		if (submode == 8) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 13;
				return;
			}
		}

		if (submode == 9) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 19;
				return;
			}
		}

		if (submode == 10) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 1;
				return;
			}
		}

		if (submode == 11) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 31;
				return;
			}
		}
		if (submode == 12) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 24;
				submode = 8;
				return;
			}
		}
		if (submode == 13) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 11;
				submode = 1;
				return;
			}
		}
		if (submode == 14) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 18;
				return;
			}
		}	
		if (submode == 15) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 21;
				submode = 3;
				return;
			}
		}
	}
	

	if (mode == 3) {
		//Line following(expexting a package)
		/*if (submode == 1 || submode == 2 || submode == 3 || submode == 4 || submode == 5) {
			if (overTarget) {
				if (!lineL && !lineR) {
					overTarget = 0;
					targets_crossed += 1;
				}
			}
			else {
				if (lineL && lineC && lineR) {
					overTarget = 1;
				}
			}
		}*/
		if (submode == 1) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 1;
				return;
			}

			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 1;
				return;
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 1;
				return;
			}
			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 1;
				return;
			}
		}

		if (submode == 3) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 2;
				return;
			}

			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 2;
				return;
			}
		}

		if (submode == 4) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 2;
				return;
			}
			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 2;
				return;
			}
		}

		if (submode == 5) {

			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 3;
				return;
			}

			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 5;
				return;
			}
		}

		if (submode == 6) {
			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 6;
				return;
			}
		}

		if (submode == 7) {
			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 7;
				return;
			}
		}

		if (submode == 8) {
			if (abs(ultrasoundDistance-ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 8;
				return;
			}
		}
	}

	if (mode == 4) {
		//Initiate grabbing an carrying
		if (submode == 1) {
			if (servoPosition == 1) {
				mode = 5;
				submode = 13;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 2) {
			if (servoPosition == 1) {
				mode = 20;
				//last_direction = direction;
				submode = 2;
				carryingBlue = 1;
				return;
			}
		}

		if (submode == 3) {
			if (servoPosition == 1) {
				mode = 5;
				submode = 13;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 4) {
			if (servoPosition == 1) {
				mode = 20;
				//last_direction = direction;
				submode = 4;
				carryingBlue = 1;
				return;
			}
		}

		if (submode == 5) {
			if (servoPosition == 1) {
				mode = 20;
				//last_direction = direction;
				submode = 5;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 6) {
			if (servoPosition == 1) {
				mode = 20;
				//last_direction = direction;
				submode = 6;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 7) {
			if (servoPosition == 1) {
				mode = 20;
				//last_direction = direction;
				submode = 7;
				carryingRed = 1;
				return;
			}
		}

		if (submode == 8) {
			if (servoPosition == 1) {
				mode = 20;
				submode = 8;
				carryingRed = 1;
				return;
			}
		}
	}

	if (mode == 5) {
		//turn around
		if (decisionTimer == 0) {
			decisionTimer = millis();
			return;
		}
		else if (submode == 13) {
			if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 9;
				submode = 3;
				return;
			}
		}
		else if (millis() - decisionTimer > 2000) {
			if (lineC) {
				decisionTimer = 0;
				if (submode == 1) {
					if (targets_crossed < 2) {
						mode = 21;
						submode = 1;
						return;
					}
					else {
						mode = 22;
						submode = 1;
						return;
					}
				}

				if (submode == 2) {
					mode = 22;
					submode = 2;
					return;
				}


				if (submode == 3) {
					mode = 1;
					submode = 22;
					return;
				}


				if (submode == 4) {
					mode = 1;
					submode = 6;
					return;
				}


				if (submode == 5) {
					mode = 2;
					submode = 14;
					return;
				}


				if (submode == 6) {
					if (targets_crossed == 0) {
						mode = 21;
						submode = 11;
						return;
					}
					else {
						mode = 22;
						submode = 11;
						return;
					}
				}


				if (submode == 7) {
					mode = 1;
					submode = 14;
					return;
				}


				if (submode == 8) {
					if (red_packages_aside < 2) {
						mode = 23;
						submode = 6;
						return;
					}
					if (red_packages_aside == 2) {
						mode = 21;
						submode = 23;
						return;
					}

				}

				if (submode == 9) {
					if (red_packages_aside < 2) {
						mode = 22;
						submode = 2;
						return;
					}
					if (red_packages_aside == 2) {
						mode = 22;
						submode = 4;
						return;
					}

				}

				if (submode == 10) {
					mode = 22;
					submode = 20;
					return;
				}

				if (submode == 11) {
					mode = 22;
					submode = 20;
					return;
				}

				if (submode == 12) {
					mode = 5;
					submode = 12;
					return;
				}
			}
		}
	}

	if (mode == 6) {
		//Turn left on junction
		if (submode == 1) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 21;
				submode = 1;
				return;
			}
		}
		if (submode == 2) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 24;
				submode = 7;
				return;
			}
		}
		if (submode == 3) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 21;
				submode = 3;
				return;
			}
		}
		if (submode == 4) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 7;
				return;
			}
		}
		if (submode == 5) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 18;
				return;
			}
		}
		if (submode == 6) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 12;
				return;
			}
		}
		if (submode == 7) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 11;
				submode = 2;
				return;
			}
		}
		if (submode == 8) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 10;
				return;
			}
		}
		if (submode == 9) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 1;
				submode = 15;
				return;
			}
		}
		if (submode == 10) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				if (blue_done == 0) {
					mode = 23;
					submode = 2;
					return;
				}

				if (blue_done == 1) {
					mode = 23;
					submode = 4;
					return;
				}
			}
		}
		if (submode == 11) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				if (targets_crossed == 0) {
					mode = 21;
					submode = 1;
					return;
				}
				else {
					mode = 22;
					submode = 1;
					return;
				}
			}
		}
		if (submode == 12) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3500) {
				decisionTimer = 0;
				mode = 2;
				submode = 13;
				return;
			}
		}
		if (submode == 13) {
			if (decisionTimer == 0) {
				if (lineC && lineR && lineL) {
					decisionTimer = millis();
					return;
				}
			}
			else if (millis() - decisionTimer > 3000) {
				decisionTimer = 0;
				mode = 2;
				submode = 14;
				return;
			}
		}

	}

	if (mode == 7) {
		//Readjust grabber position, and place package down
		if (servoPosition == 0) {
			if (submode == 1) {
				mode = 5;
				//last_direction = direction;
				submode = 5;
				carryingBlue = 0;
				blue_done += 1;
				return;
			}

			if (submode == 2) {
				mode = 15;
				//last_direction = direction;
				submode = 3;
				carryingBlue = 0;
				blue_done += 1;
				return;
			}

			if (submode == 3) {
				mode = 5;
				//last_direction = direction;
				submode = 8;
				carryingRed = 0;
				red_done += 1;
				return;
			}

			if (submode == 4) {
				mode = 5;
				//last_direction = direction;
				submode = 9;
				carryingRed = 0;
				red_done += 1;
				return;
			}
		}
	}

	if (mode == 8) {
		//Push packages ahead for 20 cm, reverse for 10cm and stop
		if (submode == 1) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 5000) {
				decisionTimer = 0;
				mode = 14;
				submode = 1;
				return;
			}
		}
	}

	if (mode == 9) {
		//Place red packages down on the line
		if (servoPosition == 0) {
			carryingRed = 0;
			if (submode == 1) {
				red_packages_aside += 1;
				mode = 5;
				//last_direction = direction;
				submode = 10;
				return;
			}

			if (submode == 2) {
				red_packages_aside += 1;
				mode = 5;
				//last_direction = direction;
				submode = 11;
				return;
			}
			if (submode == 3) {
				red_packages_aside += 1;
				mode = 15;
				submode = 6;
				return;
			}
		}
	}

	if (mode == 10) {
		//Stop 20cm beyond junction
		if (submode == 1) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 9;
				submode = 2;
				return;
			}
		}
		
	}

	if (mode == 11) {
		//Line following (expecting blue target)

		if (submode == 1) {
			if (lineC && lineL && lineR ) {
				//complete blue top
				mode = 14;
				submode = 5;
				return;
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR ) {
				//complete blue btm
				mode = 14;
				submode = 4;
				return;
			}
		}
	}

	if (mode == 12) {
		//Line following (expecting red target)
		if (submode == 1) {
			if (lineC && lineL && lineR) {
				if (targets_crossed == 0 || targets_crossed == 1) {
					//within T junction
					mode = 14;
					//last_direction = direction;
					submode = 2;
					return;
				}

				if (targets_crossed > 1) {
					mode = 19;
					submode = 3;
					return;
				}
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR ) {
				//complete red right
				mode = 14;
				submode = 3;
				return;
			}
		}
	}

	if (mode == 13) {
		//Line following (expecting the starting area)
		if (lineC && lineL && lineR) {
			if (submode == 1 || submode == 2) {
				mode = 100;
				submode = 100;
				return;
			}
		}
	}

	if (mode == 14) {
		//Run backwards
		if (submode == 1) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 9;
				submode = 1;
				return;
			}
		}
		if (submode == 2) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 7;
				submode = 3;
				return;
			}
		}
		if (submode == 3) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 7;
				submode = 4;
				return;
			}
		}
		if (submode == 4) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 7;
				submode = 2;
				return;
			}
		}
		if (submode == 5) {
			if (decisionTimer == 0) {
				decisionTimer = millis();
				return;
			}
			else if (millis() - decisionTimer > 2000) {
				decisionTimer = 0;
				mode = 7;
				submode = 1;
				return;
			}
		}
	}

	if (mode == 15) {
		//turn around (anticlockwise)
		if (decisionTimer == 0) {
			decisionTimer = millis();
			return;
		}
		else if (lineC) {
			if (millis() - decisionTimer > 4000) {

				if (submode == 1) {
					decisionTimer = 0;
					if (targets_crossed == 0) {
						mode = 21;
						submode = 16;
						return;
					}
					else {
						mode = 22;
						submode = 16;
						return;
					}
				}

				if (submode == 2) {
					decisionTimer = 0;
					if (targets_crossed == 0) {
						mode = 21;
						submode = 6;
						return;
					}
					else {
						mode = 22;
						submode = 6;
						return;
					}
				}

			}

			if (millis() - decisionTimer > 2000) {

				if (submode == 3) {
					decisionTimer = 0;
					mode = 1;
					submode = 19;
					return;
				}

				if (submode == 5) {
					decisionTimer = 0;
					mode = 2;
					submode = 13;
					return;
				}

				if (submode == 6) {
					if (blue_done == 0) {
						mode = 23;
						submode = 2;
						return;
					}

					if (blue_done == 1) {
						mode = 23;
						submode = 4;
						return;
					}
				}
			}
		}
	}

	if (mode == 16) {
		//Centre block down
		if (servoPosition == 1) {
			if (submode == 1) {
				mode = 17;
				submode = 1;
				return;
			}
			if (submode == 2) {
				mode = 17;
				submode = 2;
				return;
			}
			if (submode == 5) {
				mode = 17;
				submode = 5;
				return;
			}
			if (submode == 6) {
				mode = 17;
				submode = 6;
				return;
			}
			if (submode == 7) {
				mode = 17;
				submode = 7;
				return;
			}
			if (submode == 8) {
				mode = 17;
				submode = 8;
				return;
			}
		}
	}

	if (mode == 17) {
		//Centre block up
		if (servoPosition == 0) {
			if (submode == 1) {
				if (colour == 0) {
					mode = 4;
					submode = 1;
					return;
				}
				else {
					mode = 4;
					submode = 2;
					return;
				}
			}
			if (submode == 2) {
				if (colour == 0) {
					mode = 4;
					submode = 3;
					return;
				}
				else {
					mode = 4;
					submode = 4;
					return;
				}
			}
			if (submode == 5) {
				mode = 4;
				submode = 5;
				return;
			}
			if (submode == 6) {
				mode = 4;
				submode = 6;
				return;
			}
			if (submode == 7) {
				mode = 4;
				submode = 7;
				return;
			}
			if (submode == 8) {
				mode = 4;
				submode = 8;
				return;
			}
		}
	}

	if (mode == 18) {
		//Crossed red target, counter +1
		if (decisionTimer == 0) {
			decisionTimer = millis();
			return;
		}
		else if (millis() - decisionTimer > 2300) {
			decisionTimer = 0;
			if (submode == 1) {
				mode = 1;
				submode = 24;
				targets_crossed += 1;
				return;
			}

			if (submode == 2) {
				mode = 1;
				submode = 25;
				targets_crossed += 1;
				return;
			}

			if (submode == 3) {
				mode = 1;
				submode = 26;
				targets_crossed += 1;
				return;
			}

		}
	}

	if (mode == 19) {
		//Crossed red target, counter -1
		if (decisionTimer == 0) {
			decisionTimer = millis();
			return;
		}
		else if (millis() - decisionTimer > 2300) {
			decisionTimer = 0;
			if (submode == 1) {
				mode = 1;
				submode = 27;
				targets_crossed -= 1;
				return;
			}

			if (submode == 2) {
				mode = 1;
				submode = 28;
				targets_crossed -= 1;
				return;
			}

			if (submode == 3) {
				mode = 1;
				submode = 29;
				targets_crossed -= 1;
				return;
			}

			if (submode == 4) {
				mode = 1;
				submode = 30;
				targets_crossed -= 1;
				return;
			}

		}
	}

	if (mode == 20) {
		//Crossed red target, counter -1
		if (decisionTimer == 0) {
			decisionTimer = millis();
			return;
		}
		else if (millis() - decisionTimer > 2500) {
			decisionTimer = 0;
			if (submode == 1) {
				mode = 15;
				submode = 6;
				return;
			}

			if (submode == 2) {
				mode = 15;
				submode = 2;
				return;
			}

			if (submode == 3) {
				mode = 15;
				submode = 6;
				return;
			}

			if (submode == 4) {
				mode = 5;
				submode = 6;
				return;
			}
			if (submode == 5) {
				mode = 5;
				submode = 1;
				return;
			}
			if (submode == 6) {
				mode = 5;
				submode = 2;
				return;
			}
			if (submode == 7) {
				mode = 5;
				submode = 3;
				return;
			}
			if (submode == 8) {
				mode = 22;
				submode = 2;
				return;
			}

		}
	}

	if (mode == 21) {

		if (submode == 1) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 14;
				//last_direction = direction;
				submode = 2;
				return;
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 3;
				return;
			}
		}

		if (submode == 3) {
			if (lineC && lineL && lineR) {
				mode = 100;
				submode = 100;
				return;
			}
		}

		if (submode == 6) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 6;
				//last_direction = direction;
				submode = 4;
				return;
			}
		}

		if (submode == 11) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 6;
				//last_direction = direction;
				submode = 6;
				return;
			}
		}

		if (submode == 16) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 11;
				return;
			}
		}

		if (submode == 17) {
			if (lineL && lineC && lineR) {
				mode = 2;
				submode = 10;
				return;
			}
		}

		if (submode == 23) {
			if (lineC && lineL && lineR) {
				mode = 2;
				//last_direction = direction;
				submode = 12;
				return;
			}
		}

	}

	if (mode == 22) {

		if (submode == 1) {
			if (lineC && lineL && lineR) {
				mode = 19;
				submode = 3;
				return;
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR) {
				//complete red right
				mode = 14;
				submode = 3;
				return;
			}
		}

		if (submode == 3) {
			if (lineC && lineL && lineR) {
				//complete red right
				mode = 2;
				submode = 3;
				return;
			}
		}

		if (submode == 4) {
			if (lineC && lineL && lineR) {
				//within T junction
				mode = 2;
				//last_direction = direction;
				submode = 4;
				return;
			}
		}

		if (submode == 6) {
			if (lineC && lineL && lineR) {
				mode = 19;
				submode = 1;
				return;
			}
		}

		if (submode == 11) {
			if (lineC && lineL && lineR) {
				mode = 19;
				submode = 2;
				return;
			}
		}

		if (submode == 16) {
			if (lineC && lineL && lineR) {
				mode = 19;
				submode = 4;
				return;
			}
		}

		if (submode == 20) {
			if (lineC && lineL && lineR) {
				mode = 6;
				//last_direction = direction;
				submode = 10;
				return;
			}
		}

	}

	if (mode == 23) {

		if (submode == 1) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 1;
				return;
			}

			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 1;
				return;
			}
		}

		if (submode == 2) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 1;
				return;
			}
			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 1;
				return;
			}
		}

		if (submode == 3) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 2;
				return;
			}

			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 2;
				return;
			}
		}

		if (submode == 4) {
			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 2;
				return;
			}
			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 2;
				return;
			}
		}

		if (submode == 5) {

			if (lineC && lineL && lineR) {
				mode = 18;
				submode = 3;
				return;
			}

			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 5;
				return;
			}
		}

		if (submode == 6) {
			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 6;
				return;
			}
		}

		if (submode == 7) {
			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 7;
				return;
			}
		}

		if (submode == 8) {
			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 8;
				return;
			}
		}

	}

	if (mode == 24) {

		if (submode == 7) {
			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 7;
				return;
			}
		}

		if (submode == 8) {
			if (abs(ultrasoundDistance - ultrasoundThreshold) <= 0.3) {
				mode = 16;
				submode = 8;
				return;
			}
		}

	}

	if (mode == 100) {
		if (submode == 100) {
			//enter the start area
			if (lineC && lineR && lineL) {
				mode = 101;
				submode = 101;
				return;
			}
		}
	}

	if (mode == 101) {
		if (submode == 101) {
			//Stop at the start area

			return;
		}
	}

}