#include "MotorControl.h"

void set_motor_speeds(void) {
    //Only tell the motors to change speed if the speed variables have changed
    if (motorLspeed != Prev_motorLspeed) {
        Prev_motorLspeed = motorLspeed;

        //Set the motor speed
        motorL->setSpeed(abs(motorLspeed));

        //Set the motor direction
        if (motorLspeed > 0) {
            motorL->run(FORWARD);
        }
        else if (motorLspeed == 0) {
            motorL->run(RELEASE);
        }
        else motorL->run(BACKWARD);
    }
    if (motorRspeed != Prev_motorRspeed) {
        Prev_motorRspeed = motorRspeed;

        //Set the motor direction
        if (motorRspeed > 0) {
            motorR->run(FORWARD);
        }
        else if (motorRspeed == 0) {
            motorR->run(RELEASE);
        }
        else motorR->run(BACKWARD);

        //Set the motor speed
        motorR->setSpeed(abs(motorRspeed));
    }
}
