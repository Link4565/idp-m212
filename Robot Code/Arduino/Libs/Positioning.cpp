#include "Positioning.h"

void point_in_direction(float angle) {
	//Stubbed
	return;
}

void go_to_position(float x, float y) {
	//Stubbed
	return;
}

void set_distance_to_box(float distance, float tolerance) {
	//Move if the block is outside the tolerance region
	if (abs(distance - ultrasoundDistance) > tolerance) {
		//Move forward if too far away
		if (ultrasoundDistance > distance) {
			motorLspeed = 120;
			motorRspeed = 120;
		}
		//Move backwards if too close to block
		else {
			motorLspeed = -120;
			motorRspeed = -120;
		}
	}
	else {
		//Stop if the block is the right distance away
		motorLspeed = 0;
		motorRspeed = 0;
	}
}