#include "LineFollow.h"

void follow_line_2_sensors(void) {
    //Follow a line using only the outer sensors
    if (!lineL) {
        motorLspeed = 200;
    }
    else {
        motorLspeed = -150;
    }

    if (!lineR) {
        motorRspeed = 200;
    }
    else {
        motorRspeed = -150;
    }
}

void follow_line_left_junction_2_sensors(void) {
    //Turn left if on a line
    if (lineL) {
        motorLspeed = -170;
        motorRspeed = 200;
    }
    else {
        motorLspeed = 200;
        motorRspeed = 200;
    }
}

void follow_line_right_junction_2_sensors(void) {
    //Turn right if on a line
    if (lineR) {
        motorLspeed = 200;
        motorRspeed = -170;
    }
    else {
        motorLspeed = 200;
        motorRspeed = 200;
    }
}

void follow_line_3_sensors(void) {
    //Follow a line, keeping the centre sensor on the line, and the outer sensors off
    if (lineC) {
        if (!lineL) {
            motorLspeed = 180;
        }
        else {
            motorLspeed = -150;
        }

        if (!lineR) {
            motorRspeed = 180;
        }
        else {
            motorRspeed = -150;
        }

        if (lineL && lineR) {
            motorLspeed = 200;
            motorRspeed = 200;
        }
    }
    else {
        if (lineL) {
            motorLspeed = -150;
            motorRspeed = 180;
        }
        if (lineR) {
            motorLspeed = 180;
            motorRspeed = -150;
        }
        if (!lineL && !lineR) {
            motorLspeed = -150;
            motorRspeed = -150;
            error = true;
        }
    }
}

void follow_line_centre_sensor_left(void) {
    //Keep the right line sensor on the line, and the others off
    if (lineC || lineL) {
        motorLspeed = -191;
        motorRspeed = 200;
    }
    else {
        if (lineR) {
            motorLspeed = 200;
            motorRspeed = 200;
        }
        else {
            motorLspeed = 200;
            motorRspeed = -191;
        }
    }
}

void follow_line_centre_sensor_right(void) {
    //Keep the left line sensor on the line and the others off
    if (lineC || lineR) {
        motorLspeed = 255;
        motorRspeed = -191;
    }
    else {
        if (lineL) {
            motorLspeed = 255;
            motorRspeed = 255;
        }
        else {
            motorLspeed = -191;
            motorRspeed = 255;
        }
    }
}