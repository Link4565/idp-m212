#include "Arduino.h"
#include <Servo.h>
#include <HCSR04_with_timeout.h>
#include <Adafruit_MotorShield.h>

void init_vars(void);

//Declare all variables as global

extern Adafruit_MotorShield AFMS;
extern Adafruit_DCMotor* motorL;
extern Adafruit_DCMotor* motorR;

extern int triggerPin;
extern int echoPin;
extern UltraSonicDistanceSensor distanceSensor;

extern Servo hand;
extern Servo wrist;

extern const int lineSensorL;
extern const int lineSensorC;
extern const int lineSensorR;

extern const int interrupterL;
extern const int interrupterR;

extern const int yellowLED;
extern const int blueLED;
extern const int redLED;

extern const double robotWidth;
extern const double wheelCircumference;
extern const int encoderDivisions;

extern const int servoHandMaxAngle;
extern const int servoWristMaxAngle;
extern const int servoHandMinAngle;
extern const int servoWristMinAngle;

extern const double ultrasoundThreshold;

extern short int motorLspeed;
extern short int motorRspeed;
extern short int Prev_motorLspeed;
extern short int Prev_motorRspeed;

extern double ultrasoundReadings[10];
extern double ultrasoundDistance;
extern bool lineL;
extern bool lineC;
extern bool lineR;
extern bool encoderL;
extern bool encoderR;
extern bool prev_encoderL;
extern bool prev_encoderR;

extern int colour;
extern double colourReadings[20];

extern int red_packages_aside;
extern int blue_done;
extern int red_done;

extern bool error;

extern double xPos;
extern double yPos;
extern double direction;
extern double last_direction;

extern const double xPos_T_junc;
extern const double yPos_T_junc;
extern const double xPos_intial_junc;
extern const double yPos_intial_junc;
extern const double xPos_blue_targets_junc;
extern const double yPos_blue_targets_junc;

extern const double xPos_blue_top_target;
extern const double yPos_blue_top_target;
extern const double xPos_blue_btm_target;
extern const double yPos_blue_btm_target;
extern const double xPos_red_right_target;
extern const double yPos_red_right_target;
extern const double xPos_red_btm_target;
extern const double yPos_red_btm_target;

extern short int servoHandAngle;
extern short int servoWristAngle;
extern bool servoHandDirection;
extern bool servoWristDirection;

extern double servoTimer;
extern bool servoPosition;

extern int mode;
extern int submode;

extern double ledTimer;
extern bool yellowLEDstate;

extern double decisionTimer;

extern bool carryingRed;
extern bool carryingBlue;

extern bool overTarget;
extern int targets_crossed;

extern bool controlledByPython;


