#include "Config.h"

void move_hand(void);
void move_wrist(void);
void grab(void);
void drop(void);
void centre_block_down(void);
void centre_block_up(void);