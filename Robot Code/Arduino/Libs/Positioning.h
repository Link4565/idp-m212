#include "Config.h"

void point_in_direction(float angle);
void go_to_position(float x, float y);
void set_distance_to_box(float distance, float tolerance);