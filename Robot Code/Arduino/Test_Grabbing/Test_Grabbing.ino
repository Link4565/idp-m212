#include <Servo.h>
#include <HCSR04_with_timeout.h>
#include <Adafruit_MotorShield.h>
#include "src/Config.h"
#include "src/Sensors.h"
#include "src/MotorControl.h"
#include "src/LineFollow.h"
#include "src/Positioning.h"
#include "src/Grabbing.h"
#include "src/DecisionMaking.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor* motorL = AFMS.getMotor(1);
Adafruit_DCMotor* motorR = AFMS.getMotor(2);

int triggerPin = 7;
int echoPin = 8;
UltraSonicDistanceSensor distanceSensor(triggerPin, echoPin);

Servo hand;
Servo wrist;


void setup() {
	// put your setup code here, to run once:
	AFMS.begin();

	hand.attach(9);
	wrist.attach(10);
	hand.write(servoHandMaxAngle);
	wrist.write(servoWristMaxAngle);

	pinMode(A0, INPUT);
	pinMode(A1, INPUT);

	init_vars();

	Serial.begin(9600);
}

void loop() {
	Serial.println(servoHandAngle);
	Serial.println(servoWristAngle);
	while (servoPosition == 0) {
		grab();
		delay(0.1);
		Serial.println(servoHandAngle);
		Serial.println(servoWristAngle);
	}
	delay(1000);
	while (servoPosition == 1) {
		drop();
		delay(0.1);
		Serial.println(servoHandAngle);
		Serial.println(servoWristAngle);
	}
	delay(1000);
}
