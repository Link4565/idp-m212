#include <Servo.h>
#include <HCSR04_with_timeout.h>
#include <Adafruit_MotorShield.h>
#include <ArduinoJson.h>
#include "src/Config.h"
#include "src/Sensors.h"
#include "src/MotorControl.h"
#include "src/LineFollow.h"
#include "src/Positioning.h"
#include "src/Grabbing.h"
#include "src/DecisionMaking.h"
#include "src/MiniDecisionMaking.h"
#include "src/DecisionMakingTimer.h"
#include "src/DecisionMakingSide.h"
#include "src/DecisionMakingSideShortBlue.h"

//Connect to the motor shield
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor* motorL = AFMS.getMotor(1);
Adafruit_DCMotor* motorR = AFMS.getMotor(2);

//Set the pins for the ultrasound sensor
int triggerPin = 7;
int echoPin = 8;
UltraSonicDistanceSensor distanceSensor(triggerPin, echoPin);

//Name the individual servos
Servo hand;
Servo wrist;

int i = 0;


void setup() {
  // put your setup code here, to run once:

  //Start the motor shield
  AFMS.begin();

  //Set the servo pins and tell them to open fully
  hand.attach(9);
  wrist.attach(10);
  hand.write(servoHandMaxAngle);
  wrist.write(servoWristMaxAngle);
 
  //Set the pin modes for the colour sensor inputs, led outputs and bluetooth serial
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);

  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);

  pinMode(NINA_RESETN, OUTPUT);
  digitalWrite(NINA_RESETN, LOW);
  
  //Set all variables to their starting values
  init_vars();

  //Start serial connections
  Serial.begin(115200);
  SerialNina.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  //Read the latest values from all sensors into their respective variables
  read_sensors();

  //Change the current mode and submode when the appropriate condition has been met
  decisionmakingside();

  //Se tthe leds to their appropriate state
  display_leds();

  //Camera code - commented due to lack of testing before competition

  /*
  i+=1;
  
	display_leds();

  if (i % 50 == 0){
    //Wait until data is sent from the computer
    if (SerialNina.available()) {
		  controlledByPython = true;
    }
	}
	//Stop the main robot functionality if being controlled by python from the computer
	if (!controlledByPython) {
    read_sensors();
		decisionmakingtimer();
		//decisionmaking();
	}
	else {

		//Read the latest serial data
		String  payload;
		if (SerialNina.available()) {
			payload = Serial.readStringUntil('\n');
		}

		//Create a blank json document
		StaticJsonDocument<200> doc;

		//Attempt to decode the latest received string
		DeserializationError error = deserializeJson(doc, payload);
		//Ignore data if invalid
		if (error) {
			Serial.println(error.c_str());
			return;
		}
		//Load motor speeds from json if no modes are sent
		if (doc["s"][0] == "-1") {
			motorLspeed = doc["m"][0];
			motorRspeed = doc["m"][1];
		}
		//Load the received mode and return to the main program in this mode
		else {
			mode = doc["s"][0];
			submode = doc["s"][1];
			controlledByPython = false;
		}
	}
 */

	//Switch statement to control what action each mode does

	switch (mode) {
		case 0:
			//Roll out of the start box
			hand.write(servoHandMaxAngle);
			wrist.write(servoWristMaxAngle);
			motorLspeed = 200;
			motorRspeed = 200;
			break;
		case 1:
			//Follow along the centre of a line
			follow_line_3_sensors();
			break;
		case 2:
			//Turn right at a junction
			follow_line_right_junction_2_sensors();
			break;
		case 3:
			//Follow along a line centrally and position infront of a block
			hand.write(servoHandMaxAngle);
			wrist.write(servoWristMaxAngle);
			follow_line_3_sensors();
			if (ultrasoundDistance < 12) {
				set_distance_to_box(ultrasoundThreshold, 0.2);
			}
			break;
		case 4:
			//Grab the block infront
			motorLspeed = 0;
			motorRspeed = 0;
			grab();
			break;
		case 5:
			//Turn clockwise
			motorLspeed = 200;
			motorRspeed = -200;
			break;
		case 6:
			//Turn left at a junction
			follow_line_left_junction_2_sensors();
			break;
		case 7:
			//Drop the block being carried
			motorLspeed = 0;
			motorRspeed = 0;
			drop();
			break;
		case 8:
			//Drive straight forwards
			motorLspeed = 200;
			motorRspeed = 200;
			break;
		case 9:
			//Drop the block being carried
			motorLspeed = 0;
			motorRspeed = 0;
			drop();
			break;
		case 10:
			//Drive straight forwards
			motorLspeed = 200;
			motorRspeed = 200;
			break;
		case 11:
			//Follow along the centre of a line
			follow_line_3_sensors();
			break;
		case 12:
			//Follow along the centre of a line
			follow_line_3_sensors();
			break;
		case 13:
			//Follow along the centre of a line
			follow_line_3_sensors();
			break;
		case 14:
			//Reverse
			motorLspeed = -150;
			motorRspeed = -150;
			break;
		case 15:
			//Turn anticlockwise
			motorLspeed = -200;
			motorRspeed = 200;
			break;
		case 16:
			//Lower the arms to centre a block infront of the robot
			motorLspeed = 0;
			motorRspeed = 0;
			centre_block_down();
			break;
		case 17:
			//Raise the arms after centring the block infront
			motorLspeed = 0;
			motorRspeed = 0;
			centre_block_up();
			break;
		case 18:
			//Drive straight forwards
			motorLspeed = 200;
			motorRspeed = 200;
			break;
		case 19:
			//Drive straight forwards
			motorLspeed = 200;
			motorRspeed = 200;
			break;
		case 20:
			//Drive straight forwards
			motorLspeed = 200;
			motorRspeed = 200;
			break;
		case 21:
			//Follow along the left side of a line
			follow_line_centre_sensor_left();
			break;
		case 22:
			//Follow along the right side of a line
			follow_line_centre_sensor_right();
			break;
		case 23:
			//Follow along the left of a line and position infront of a block
			hand.write(servoHandMaxAngle);
			wrist.write(servoWristMaxAngle);
			follow_line_centre_sensor_left();
			if (ultrasoundDistance < 12) {
				set_distance_to_box(ultrasoundThreshold, 0.2);
			}
			break;
		case 24:
			//Follow along the right of a line and position infront of a block
			hand.write(servoHandMaxAngle);
			wrist.write(servoWristMaxAngle);
			follow_line_centre_sensor_right();
			if (ultrasoundDistance < 12) {
				set_distance_to_box(ultrasoundThreshold, 0.2);
			}
			break;
		case 100:
			//Drive straight forwards
			motorLspeed = 200;
			motorRspeed = 200;
			break;
		case 101:
			//Stop
			motorLspeed = 0;
			motorRspeed = 0;
			break;

	}

	//Set the motor speeds to the values of motorLspeed and motorRspeed
	set_motor_speeds();

	//Serial.println(encoderL);
	//Serial.println(direction);
	
	Serial.println(ultrasoundDistance);
	//Serial.println(motorLspeed);
	//Serial.println(motorRspeed);
  
	Serial.println("Lines:");
	Serial.print(lineL);
	Serial.print(lineC);
	Serial.println(lineR);
	Serial.print("Distance");
	Serial.println(ultrasoundDistance);

	Serial.println(mode);
	Serial.println(submode);
	Serial.print("Crossed boxes");
	Serial.println(targets_crossed);
	//Serial.println(colour);
}
